/**
 * @file spi.c
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 */

/* -- Includes -- */

/* AVR specifik includes */
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "spi.h"


#if USE_IRQ == 1

/*! @brief  Data input register buffer.
 *
 *  Incoming bytes are stored in this byte until the next transfer is complete.
 *  This byte can be used the same way as the SPI data register in the native
 *  SPI module, which means that the byte must be read before the next transfer
 *  completes and overwrites the current value.
 */
volatile unsigned char storedUSIDR;

/*! @brief  Driver status bit structure.
 *
 *  This struct contains status flags for the driver.
 *  The flags have the same meaning as the corresponding status flags
 *  for the native SPI module. The flags should not be changed by the user.
 *  The driver takes care of updating the flags when required.
 */
struct usidriverStatus_t
{
    unsigned char masterMode : 1;       //!< True if in master mode.
    unsigned char transferComplete : 1; //!< True when transfer completed.
    unsigned char writeCollision : 1;   //!< True if put attempted during transfer.
};

volatile struct usidriverStatus_t spiX_status; //!< The driver status bits.

#if defined(__AVR_ATtiny2313__)

/**
 *  @brief  Timer/Counter 0 Compare Match Interrupt handler.
 *
 *  This interrupt handler is only enabled when transferring data
 *  in master mode. It toggles the USI clock pin, i.e. two interrupts
 *  results in one clock period on the clock pin and for the USI counter.
 * 
 */
ISR(TIMER0_COMPA_vect)
{
    USICR |= (1 << USITC);
}

/** 
 *  @brief  USI Timer Overflow Interrupt handler.
 *
 *  This handler disables the compare match interrupt if in master mode.
 *  When the USI counter overflows, a byte has been transferred, and we
 *  have to stop the timer tick.
 *  For all modes the USIDR contents are stored and flags are updated.
 *     
 *  Master must now disable the compare match interrupt to prevent more USI counter clocks.
 */
ISR(USI_OVERFLOW_vect)
{
    
    if (spiX_status.masterMode == 1)
    {
        TIMSK &= ~(1 << OCIE0A);
    }
    
    USISR = _BV(USIOIF);  //< Update flags and clear USI counter
    spiX_status.transferComplete = 1;
    // Copy USIDR to buffer to prevent overwrite on next transfer.
    storedUSIDR = USIDR;
}

static inline void spi_wait(void)
{
    do
    {

    } while (spiX_status.transferComplete == 0);
}

#endif //AVR_TYPE
#endif //USE_IRQ


void init_SPI(uint8_t spi_mode)
{
#if defined(__AVR_ATtiny167__)
    DDR_SPI |= ((1 << DD_MOSI) | (1 << DD_SCK) | (1 << DD_SS)); 
    SPCR |= (0 << SPIE) |                                       
            (1 << SPE) |                                        
            (0 << DORD) |                                      
            (1 << MSTR) |                                       
            (0 << CPOL) |                                     
            (0 << CPHA) |                                       
            (1 << SPR0) |                                       
            (0 << SPR1);                                       
    SPSR |= (1 << SPI2X);                                      
#elif defined(__AVR_ATtiny2313__)

    USI_DIR_REG |= (1 << USI_DATAOUT_PIN) | (1 << USI_CLOCK_PIN); 
    USI_DIR_REG &= ~(1 << USI_DATAIN_PIN);                        
    USI_OUT_REG |= (1 << USI_DATAIN_PIN);                         

#if USE_IRQ == 1
   
    USICR = (1 << USIOIE) | (1 << USIWM0) |
            (1 << USICS1) | (spi_mode << USICS0) |
            (1 << USICLK);

    TCCR0A = (1 << WGM01);
    TCCR0B = TC0_PS_SETTING;
   
    OCR0A = TC0_COMPARE_VALUE;
   

    spiX_status.masterMode = 1;
    spiX_status.transferComplete = 0;
    spiX_status.writeCollision = 0;
    storedUSIDR = 0;
#elif USE_IRQ == 0
    USICR |= ((1 << USIWM0) | (1 << USICS1) | (spi_mode << USICS0) | (1 << USICLK)); 
#endif
#endif
}



uint8_t SPI_WriteReadByte(uint8_t data)
{
#if defined(__AVR_Attiny167__)
    SPDR = data; 
    while (!(SPSR & (1 << SPIF)))
        ;
    return SPDR; 
#elif defined(__AVR_ATtiny2313__)
#if USE_IRQ == 1
    SPI_WriteByte(data);
    return storedUSIDR;
#elif USE_IRQ == 0
    SPI_WriteByte(data);
    return USIDR;
#endif
#endif
}


uint8_t SPI_WriteByte(uint8_t data)
{
#if defined(__AVR_Attiny167__)
    SPDR = data;
    while (!(SPSR & (1 << SPIF)))
        ; 
#elif defined(__AVR_ATtiny2313__)
#if USE_IRQ == 1

    if ((USISR & 0x0F) != 0)
    {
        
        spiX_status.writeCollision = 1;
        return 0;
    }
  
    spiX_status.transferComplete = 0;
    spiX_status.writeCollision = 0;
    
    USIDR = data;
    // Master should now enable compare match interrupts.
    if (spiX_status.masterMode == 1)
    {
        TIFR |= (1 << OCF0A);   // Clear compare match flag.
        TIMSK |= (1 << OCIE0A); // Enable compare match interrupt.
    }
    if (spiX_status.writeCollision == 0)
    {
        spi_wait();
        return 1;
    }
    return 0;
#elif USE_IRQ == 0
    USIDR = data; //Write byte to register

    USISR |= _BV(USIOIF); // clear flag to receive new data;

    while ((USISR & (1 << USIOIF)) == 0) //Toggling SCK ppin using 4-bit counter to send tada to slave.
    {
        USICR |= _BV(USITC);
    }
    return 1;
#endif
#endif
}


void SPI_WriteDataBuffer(uint8_t *data_buffer_out, uint8_t length)
{
#if defined(__AVR_Attiny167__)
    uint8_t *pointer;            //declare pointer
    uint8_t i;                   //declare indexing variable
    pointer = data_buffer_out;   //pointer is showing on first byte of "data_buffer_out"
    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*pointer++); //send bytes to nRF
    }

#elif defined(__AVR_ATtiny2313__)
    uint8_t *pointer;          //declare pointer
    uint8_t i;                 //declare indexing variable
    pointer = data_buffer_out; //pointer is showing on first byte of "data_buffer_out"

    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*pointer++); //send bytes to nRF
    }
#endif
}


void SPI_WriteReadDataBuffer(uint8_t *data_buffer_in, uint8_t *data_buffer_out, uint8_t length)
{
#if defined(__AVR_Attiny167__)
    uint8_t *pointer1; //declare pointer1
    uint8_t *pointer2; //declare pointer2
    uint8_t i;         //declare indexing variable

    pointer1 = data_buffer_out;  //pointer1 is showing on first byte of "data_buffer_out"
    pointer2 = data_buffer_in;   //pointer2 is showing on first byte of "data_buffer_in"
    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        *pointer2++ = SPI_WriteReadByte(*pointer1++); //send bytes to nRF and read data back from nRF
    }

#elif defined(__AVR_ATtiny2313__)
#if USE_IRQ == 1
    uint8_t *pointer1; //declare pointer1
    uint8_t *pointer2; //declare pointer2
    uint8_t i;         //declare indexing variable

    pointer1 = data_buffer_out; //pointer1 is showing on first byte of "data_buffer_out"
    pointer2 = data_buffer_in;  //pointer2 is showing on first byte of "data_buffer_in"

    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*pointer1++); //send bytes to nRF and read data back from nRF
        *pointer2++ = storedUSIDR;
    }
#elif USE_IRQ == 0
    uint8_t *pointer1; //declare pointer1
    uint8_t *pointer2; //declare pointer2
    uint8_t i;         //declare indexing variable

    pointer1 = data_buffer_out; //pointer1 is showing on first byte of "data_buffer_out"
    pointer2 = data_buffer_in;  //pointer2 is showing on first byte of "data_buffer_in"

    for (i = 0; i < length; i++) //until "i" is smaller than "length"
    {
        SPI_WriteByte(*pointer1++); //send bytes to nRF and read data back from nRF
        *pointer2++ = USIDR;
    }
    
#endif
#endif
}
