#pragma once

/**
 * @file spi.h
 * @author Grzegorz Rezmer <grzes_rezmer AT o2.pl>
 * @version 1.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * https://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 *  This is my implementation of SPI AVR library. I Will try to add support for 
 *  other microcontrolers sutch as ATtiny85 or ATtiny2313 becouse they use USI modlue
 *  to communicate over SPI. 
 * 
 *  This Library also can by configurated to use IRQ or normal PIN mode.
 *  
 * 
 */

/*!
 *  @brief  Use IRQ definition
 *
 *  Set USE_IRQ as 1 or 0 to configure librrary to be used with interupts
 *  or normal USI mode. 
 * 
 * 1 - USI over IRQ fandler ON 
 * 0 - USI uver IRQ handler OFF
*/
#define USE_IRQ  1

/*! @def SPI pin definitions 
 *  @brief  DEFINE SPI PIn on AVR hardware. 
*/
#define USI_OUT_REG PORTB       //!< USI port output register.
#define USI_IN_REG PINB         //!< USI port input register.
#define USI_DIR_REG DDRB        //!< USI port direction register.
#define USI_CLOCK_PIN DD_SCK    //!< USI clock I/O pin.
#define USI_DATAIN_PIN DD_DI  //!< USI data input pin.
#define USI_DATAOUT_PIN DD_DO //!< USI data output pin.


#if USE_IRQ == 1

/*!
 *  @brief  SETUP IRQ prescaler and compare value 
 *
 *      Speed configuration:
 *      Bits per second = CPUSPEED / PRESCALER / (COMPAREVALUE+1) / 2.
 *      Maximum = CPUSPEED / 64.
 * 
 *      Must be 1, 8, 64, 256 or 1024.
 */
#define TC0_PRESCALER_VALUE  8 

/*!
 *  @brief  SETUP IRQ prescaler and compare value. 
 *
 *      Speed configuration:
 *      Bits per second = CPUSPEED / PRESCALER / (COMPAREVALUE+1) / 2.
 *      Maximum = CPUSPEED / 64.
 * 
 *      Must be 0 to 255. Minimum 31 with prescaler CLK/1.
 * 
 */
#define TC0_COMPARE_VALUE  200 

//@cond HIDDEN_SYMBOLS
#if TC0_PRESCALER_VALUE == 1
#define TC0_PS_SETTING (1 << CS00)
#elif TC0_PRESCALER_VALUE == 8
#define TC0_PS_SETTING (1 << CS01)
#elif TC0_PRESCALER_VALUE == 64
#define TC0_PS_SETTING (1 << CS01) | (1 << CS00)
#elif TC0_PRESCALER_VALUE == 256
#define TC0_PS_SETTING (1 << CS02)
#elif TC0_PRESCALER_VALUE == 1024
#define TC0_PS_SETTING (1 << CS02) | (1 << CS00)
#else
#error Invalid T/C0 prescaler setting.
#endif
#endif
//@endcond

/*! 
*   @brief USI SETUP Function. 
 *
 *  This is first function that has to be call. 
 *  Function confugure microcontroler registers and 
 *  pins to work as USI module.
 * 
 *  @param spi_mode sets usi module to work as:   
 *      spi_mode: 0 - external clock - positive edge, 
 *                1 - external clock - negative edge.
 *  @return Void.  
*/
void init_SPI(uint8_t spi_mode);                                                                      

/*! 
*   @brief Write/Read byte to slave.  
 *
 *  This is first function sends one Byte to slave device,
 *  and read register containing incomming data from slave. 
 * 
 *  @param data should by a Byte that you want to send to slave. 
 * 
 *  @return it returns Byte send from slave device or if communication 
 *          was not correct it will return sending byte.   
*/
uint8_t SPI_WriteReadByte(uint8_t data);                                                     

/*! 
*   @brief Write/Read byte to slave.  
 *
 *  This is first function sends one Byte to slave device,
 *  and return value 1 if succes or 0 if there was an error.  
 * 
 *  @param data should by a Byte that you want to send to slave. 
 * 
 *  @return it returns status byte. If Succes it will be 1 if failure it will
 *          be 0.    
*/
uint8_t  SPI_WriteByte(uint8_t data);                                                              

/** @brief Write Read buffer to slave device. 
 *
 *  This function send buffer data to slave and fill passed buffor with
 *  returning data from slave device.   
 * 
 *  @param data_buffer_out Pointer to buffer array with data to send.
 *  @param length Lengh of passed buffer. 
 *  @return Void.
*/
void SPI_WriteDataBuffer(uint8_t *data_buffer_out, uint8_t length);                            

/*! 
*   @brief Write/Read buffer to slave device.  
 *
 *  This function send buffer data to slave and fill passed buffor with
 *  returning data from slave device.   
 * 
 *  @param data_buffer_in Pointer to buffer array with data to send.
 *  @param data_buffer_out Pointer to buffer array with data received from slave
 *                                      Important this and sending buffer schould have thisame size.
 *  @param length Lengh of passed buffer. 
 * 
 *  @return It dont direcly return value but write received data to buffer array.     
*/
void SPI_WriteReadDataBuffer(uint8_t *data_buffer_in, uint8_t *data_buffer_out, uint8_t length);
