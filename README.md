Main Page {#mainpage}
=========

### General Information 

This library is make for <b>AVR Microcontrollers</b> like ATtiny or ATMega. Full documentation is avaible on this addres: [Link][1] or offline in PDF file named "Documentation.pdf".

References to SPI driver development: 

* [AVR for beginners][2]
* [AVR SPI documentation][3]
* [AVR USI decumentation][4]

## Configuration

To configure library you will need to chose operation mode by setting 1 or 0 to `USE_IRQ` define. I you want to use operation over IRQ pleas see documentation reference how to set `TC0_PRESCALER_VALUE` and `TC0_COMPARE_VALUE`. 

Next step is to setup a PIN configuration over couple of defines as example below:

```
USI_OUT_REG PORTB
USI_IN_REG PINB 
USI_DIR_REG DDRB
USI_CLOCK_PIN PB1
USI_DATAIN_PIN PB2
USI_DATAOUT_PIN PB3
```
This lib will menage all pin configuration durning init finction. 

### Suported devices 

List of suported AVR devices: 

Device Type | Name |  
-------------|:-----:|
ATtiny | ATtiny85, ATtiny2313, ATtiny167 |
ATMega | ATmega328 |  


## Author 

My name is <b>Grzegorz Rezmer</b> and I'm intrested in microcontrollers development. Moust of my time I'm writing C nad C++ code for <b>GNU/LINUX</b>, <b>AVR</b> and <b>ESP32(or 8266)</b>. From 2018 I'm working in software development buissnes. I'm trying to write my code in best way i Know but if you have some suggestions or you will find a bug please kontakt my. 

Email: "grzes_rezmer AT o2 . pl"

## Licence

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details at
https://www.gnu.org/copyleft/gpl.html

### END

[1]: https://avr_libs.gitlab.io/spi_avr/index.html
[2]: http://avrbeginners.net/architecture/spi/spi.html
[3]: https://ww1.microchip.com/downloads/en/AppNotes/Atmel-2585-Setup-and-Use-of-the-SPI_ApplicationNote_AVR151.pdf
[4]: http://ww1.microchip.com/downloads/en/AppNotes/Atmel-2582-Using-the-USI-Module-for-SPI-Communication-on-tinyAVR-and-megaAVR-Devices_ApplicationNote_AVR319.pdf